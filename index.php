<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Winro Banquet Hall - Sri Lanka</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <meta name="description" content="Welcome to Winro Banquet Hall - your premier choice for elegant and memorable events in Delgoda,Gampaha. Our exquisite banquet hall is perfect for weddings, corporate gatherings, and special occasions. Experience top-notch service and a stunning venue that will make your event unforgettable. Contact us today to plan your dream event.">

    <meta name="keywords" content="Winro Banquet Hall, banquet hall, event venue, weddings, corporate events, parties, special occasions, Delgoda, Gampaha, catering">

    <!-- Include Font Awesome CDN for icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            text-align: center;
        }
        .header {
            background-color: #F8F8F8;
            padding: 20px;
        }
        .logo {
            max-width: 200px;
            display: block;
            margin: 0 auto; /* Center the logo */
        }
        .contact-info {
            margin-top: 20px;
        }
        .contact-info p {
            margin: 10px 0;
        }
        .img {
            max-width: 600px ;
            display: block;
            margin: 0 auto; /* Center the logo */
        }
        .img_div {
            background-color: #F8F8F8;
            padding: 20px;
        }
        .centered-paragraph {
            text-align: center;
            margin-top: 20px;
        }

        .facebook-link {
            text-decoration: none;
            color: #1877f2; /* Facebook blue color */
        }

        .rounded-image {
            border-radius: 18px; /* Adjust the radius to control the amount of rounding */
            height: auto;
            display: block;
            margin: 0 auto; /* Center the image horizontally */
        }

    </style>
</head>
<body>
    <div class="header">
        <!-- Your logo with Font Awesome icon -->
        <img src="logo.jpg" alt="Winro Banquet Hall Logo" class="logo">
    </div>
    <div class="img_div">
        <!-- Your logo with Font Awesome icon -->
        <img src="image.jpg" alt="Winro Banquet Hall Card" class="img rounded-image">
    </div>
    <div class="centered-paragraph">
        <p>A banquet hall designed to host special occasions. Such as Weddings, Parties & Coporate meetings..</p>
    </div>
    <div class="contact-info">
        <h1>Contact Us</h1>
        <!-- Phone icon with clickable phone number -->
        <p><i class="fas fa-phone"></i> <a href="tel:+9477 174 0767">077 174 0767</a></p>
        <!-- Envelope icon with clickable email address -->
        <p><i class="fas fa-envelope"></i> <a href="mailto:info@winrobanquet.com">info@winrobanquet.com</a></p>
        <!-- Map marker icon with address -->
        <p><i class="fas fa-map-marker"></i> 92/B/1, Udupila, Delgoda 11700</p>
    </div>
    <div class="centered-paragraph">
        <p>Follow us on <a target="_blank" class="facebook-link" href="https://web.facebook.com/profile.php?id=100088670380284">Facebook <i class="fab fa-facebook"></i></a></p>
    </div>
    
</body>
</html>
